# TODO++ Full-Stack App

## Overview

TODO++ is an advanced task management web application built using Angular for the frontend and Express.js for the backend. This project aims to provide users with a comprehensive solution for organizing tasks and boosting productivity.

## Features

### Frontend

-   **User Authentication**: Secure user authentication and authorization system to ensure data privacy and security.
-   **Task Management**: Create, edit, and delete tasks with ease. Organize tasks by categories, priority, due dates, and more.
-   **Task Boards**: Boards to store your task and to filter them.
-   **Advanced Filters**: Utilize advanced filtering options to quickly find tasks based on different criteria.
-   **Drag-and-Drop**: Intuitive drag-and-drop functionality for effortless task management and reordering.

### Backend

-   **RESTful API**: Well-defined and documented RESTful API endpoints for seamless communication between frontend and backend.
-   **Database Integration**: Integration with a robust database system (e.g., MongoDB, PostgreSQL) for efficient data storage and retrieval.
-   **Authentication Middleware**: Implement authentication middleware to secure API endpoints and authenticate users.
-   **Scalability**: Design the backend system with scalability in mind to handle a growing user base and increased workload.

## Installation

1. Clone the repository: `git clone https://gitlab.com/oleksandryanov/angular-todo-app.git`
2. Navigate to the project directory: `cd angular-todo-app`
3. Set up the environmental variables as shown in this [TEMPLATE](.env.template)
4. Install dependencies: `npm run all:install`
5. Start the project: `npm run dev`

## Contributing

Contributions are welcome! If you have any suggestions, bug fixes, or new features to add, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

For any inquiries or feedback, please contact me via email: oleksandr.yanov.eu@gmail.com.
