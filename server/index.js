const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const path = require('path');

require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });

const appRoutes = require('./routes');

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));
app.use(cookieParser());

app.use('/api', appRoutes);

if (process.env.NODE_ENV === 'production') {
	app.use('/', express.static(path.join(__dirname, 'client', 'build')));

	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

const PORT = process.env.PORT || 8080;

const start = async () => {
	try {
		await mongoose.connect(process.env.MONGO_URI);

		app.listen(PORT, () => console.log(`Server started at port: ${PORT}`));
	} catch (e) {
		console.log(e);
	}
};

start();
