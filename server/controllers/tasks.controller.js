const { validationResult } = require('express-validator');

const Board = require('../models/Board');
const Task = require('../models/Task');

const getAllTasks = async (req, res) => {
	try {
		const board = await Board.findById(req.params.board_id).populate(
			'tasks'
		);

		if (board.user_id.toString() !== req.user.id)
			return res
				.status(403)
				.json({ message: 'This board is not yours(' });

		return res.json({
			tasks: board.tasks.filter((task) => !task.is_archived),
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const getTaskById = async (req, res) => {
	try {
		const task = await Task.findById(req.params.task_id);

		if (task.user_id !== req.user.id)
			return res.status(403).json({ message: 'This task is not yours(' });

		if (task.board !== req.params.board_id)
			return res
				.status(404)
				.json({ message: 'This task is not in this board(' });

		return res.json({ task });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const postNewTask = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const board = await Board.findById(req.params.board_id);

		if (board.user_id.toString() !== req.user.id)
			return res
				.status(403)
				.json({ message: 'This board is not yours(' });

		const task = new Task({
			user_id: board.user_id,
			status: req.body.status,
			name: req.body.name,
			board: board._id,
		});

		board.tasks.push(task._id);

		await task.save();
		await board.save();

		return res.json({
			message: 'Task created successfully',
			board: await board.populate('tasks'),
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const updateTaskInfo = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const task = await Task.findById(req.params.task_id);

		if (task.user_id.toString() !== req.user.id)
			return res.status(403).json({ message: 'This task is not yours(' });

		if (task.board.toString() !== req.params.board_id)
			return res
				.status(404)
				.json({ message: 'This task is not in this board(' });

		task.name = req.body.name;

		await task.save();

		const board = await Board.findById(task.board).populate('tasks');

		return res.json({ message: 'Task updated successfully', board });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const deleteTask = async (req, res) => {
	try {
		const task = await Task.findById(req.params.task_id);

		if (task.user_id.toString() !== req.user.id)
			return res.status(403).json({ message: 'This task is not yours(' });

		if (task.board.toString() !== req.params.board_id)
			return res
				.status(404)
				.json({ message: 'This task is not in this board(' });

		await task.delete();

		const board = await Board.findById(task.board).populate('tasks');

		return res.json({ message: 'Task deleted successfully', board });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const setNewTaskStatus = async (req, res) => {
	try {
		const task = await Task.findById(req.params.task_id);

		if (task.user_id.toString() !== req.user.id)
			return res.status(403).json({ message: 'This task is not yours(' });

		if (task.board.toString() !== req.params.board_id)
			return res
				.status(404)
				.json({ message: 'This task is not in this board(' });

		if (task.status === req.body.status)
			return res.status(304).json({ message: 'Nothing to change' });

		task.status = req.body.status;

		await task.save();

		const board = await Board.findById(task.board).populate('tasks');

		return res.json({ message: 'Task status updated successfuly', board });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const archiveTask = async (req, res) => {
	try {
		const task = await Task.findById(req.params.task_id);

		if (task.user_id.toString() !== req.user.id)
			return res.status(403).json({ message: 'This task is not yours(' });

		if (task.board.toString() !== req.params.board_id)
			return res
				.status(404)
				.json({ message: 'This task is not in this board(' });

		task.is_archived = !task.is_archived;

		await task.save();

		const board = await Board.findById(task.board).populate('tasks');

		return res.json({
			message: 'Task archived/disarchived successfuly',
			board,
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const addTaskComment = async (req, res) => {
	try {
		const task = await Task.findById(req.params.task_id);

		if (task.user_id.toString() !== req.user.id)
			return res.status(403).json({ message: 'This task is not yours(' });

		if (task.board.toString() !== req.params.board_id)
			return res
				.status(404)
				.json({ message: 'This task is not in this board(' });

		task.comments.push({ comment: req.body.comment });

		await task.save();

		const board = await Board.findById(task.board).populate('tasks');

		return res.json({ message: 'Comment added successfuly', board });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const deleteTaskComment = async (req, res) => {
	try {
		const task = await Task.findById(req.params.task_id);

		if (task.user_id.toString() !== req.user.id)
			return res.status(403).json({ message: 'This task is not yours(' });

		if (task.board.toString() !== req.params.board_id)
			return res
				.status(404)
				.json({ message: 'This task is not in this board(' });

		if (task.comments.some(({ _id }) => _id === req.params.comment_id))
			return res.status(404).json({ message: 'Comment was not found' });

		task.comments = task.comments.filter(
			({ _id }) => _id.toString() !== req.params.comment_id
		);

		await task.save();

		const board = await Board.findById(task.board).populate('tasks');

		return res.json({ message: 'Comment deleted successfuly', board });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

module.exports = {
	getAllTasks,
	getTaskById,
	postNewTask,
	updateTaskInfo,
	deleteTask,
	setNewTaskStatus,
	archiveTask,
	addTaskComment,
	deleteTaskComment,
};
