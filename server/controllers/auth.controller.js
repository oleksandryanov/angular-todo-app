const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');

const generateJWT = require('../services/generateJWT');
const User = require('../models/User');

const register = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const { email, password } = req.body;

		const candidate = await User.findOne({ email });

		if (candidate) {
			return res.status(400).json({ message: 'User already exists' });
		}

		const hashedPassword = await bcrypt.hash(password, 12);
		const user = new User({ email, password: hashedPassword });

		await user.save();

		return res.status(200).json({ message: 'User created' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const login = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const { email, password } = req.body;

		const user = await User.findOne({ email });

		if (!user) {
			return res.status(400).json({ message: 'User not found' });
		}

		const isMatch = await bcrypt.compare(password, user.password);

		if (!isMatch) {
			return res
				.status(400)
				.json({ message: 'Incorrect password, please try again' });
		}

		const token = generateJWT(user.email, user.id, user.role);

		return res
			.cookie('access_token', token, {
				httpOnly: true,
				secure: process.env.NODE_ENV === 'production',
				maxAge: 60 * 60 * 2 * 1000,
			})
			.status(200)
			.json({ message: 'Logged in successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const logout = async (req, res) => {
	return res
		.clearCookie('access_token')
		.status(200)
		.json({ message: 'Logged out successfully' });
};

const getAuth = async (req, res) => {
	try {
		let token = req.cookies['access_token'];

		if (!token) throw new Error('Unauthorized');

		return res.status(200).json({ message: 'Authorized' });
	} catch (e) {
		return res.status(401).json({ message: e.message });
	}
};

module.exports = { register, login, logout, getAuth };
