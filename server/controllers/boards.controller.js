const { validationResult } = require('express-validator');

const Task = require('../models/Task');
const Board = require('../models/Board');

const getAllBoards = async (req, res) => {
	try {
		const boards = await Board.find({ user_id: req.user.id }).populate(
			'tasks'
		);

		return res.json({ boards });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const getBoardById = async (req, res) => {
	try {
		const board = await Board.findById(req.params.board_id).populate(
			'tasks'
		);

		if (board.user_id.toString() !== req.user.id)
			return res
				.status(404)
				.json({ message: 'This board is not yours(' });

		return res.json({ board });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const postNewBoard = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const board = new Board({
			name: req.body.name,
			description: req.body.description,
			user_id: req.user.id,
		});

		await board.save();

		return res.json({ board, message: 'Board created successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const updateBoardInfo = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const board = await Board.findById(req.params.board_id);

		if (board.user_id.toString() !== req.user.id) {
			return res
				.status(404)
				.json({ message: 'This board is not yours(' });
		}

		board.name = req.body.name;

		await board.save();

		return res.json({ board, message: 'Board updated successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const deleteBoard = async (req, res) => {
	try {
		const board = await Board.findById(req.params.board_id);

		if (board.user_id.toString() !== req.user.id)
			return res
				.status(404)
				.json({ message: 'This board is not yours(' });

		await board.delete();

		board.tasks.forEach(async (task) => await Task.findByIdAndDelete(task));

		return res.json({ message: 'Board deleted successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const changeColumnColor = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const board = await Board.findById(req.params.board_id).populate(
			'tasks'
		);

		if (board.user_id.toString() !== req.user.id)
			return res
				.status(404)
				.json({ message: 'This board is not yours(' });

		board.colors = { ...board.colors, [req.body.column]: req.body.color };

		await board.save();

		return res.json({
			message: 'Board column color updated successfully',
			board,
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

module.exports = {
	getAllBoards,
	getBoardById,
	postNewBoard,
	updateBoardInfo,
	deleteBoard,
	changeColumnColor,
};
