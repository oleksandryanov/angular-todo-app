const { Schema, model, Types } = require('mongoose');

const schema = new Schema({
	name: { type: String, required: true },
	user_id: { type: Types.ObjectId, required: true, ref: 'User' },
	description: { type: String, default: '' },
	created_date: { type: Date, default: () => new Date() },
	tasks: [{ type: Types.ObjectId, ref: 'Task' }],
	colors: {
		TD: { type: String, default: '' },
		IP: { type: String, default: '' },
		D: { type: String, defalt: '' },
	},
});

module.exports = model('Board', schema);
