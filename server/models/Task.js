const { Schema, model, Types } = require('mongoose');

const statuses = ['TD', 'IP', 'D'];

const schema = new Schema({
	name: { type: String, required: true },
	status: { type: String, enum: statuses, required: true },
	board: { type: Types.ObjectId, required: true, ref: 'Board' },
	is_archived: { type: Boolean, default: false },
	created_date: { type: Date, default: () => new Date() },
	user_id: { type: Types.ObjectId, required: true, ref: 'User' },
	comments: [
		{
			comment: String,
			created_date: { type: Date, default: () => new Date() },
		},
	],
});

module.exports = model('Task', schema);
