const jwt = require('jsonwebtoken');

module.exports = (username, id, role) =>
	jwt.sign({ username, id, role }, process.env.JWT_SECRET, {
		expiresIn: '2h',
	});
