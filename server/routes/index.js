const router = require('express').Router();

const authMiddleware = require('../middleware/auth.middleware');

const authRoutes = require('./auth.routes');
const boardsRoutes = require('./boards.routes');
const tasksRoutes = require('./tasks.routes');

router.use('/auth', authRoutes);
router.use('/boards', authMiddleware, boardsRoutes);
router.use('/boards', authMiddleware, tasksRoutes);

module.exports = router;
