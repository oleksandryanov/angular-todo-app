const express = require('express');

const router = express.Router();

const {
	getAllTasks,
	getTaskById,
	postNewTask,
	deleteTask,
	updateTaskInfo,
	setNewTaskStatus,
	archiveTask,
	addTaskComment,
	deleteTaskComment,
} = require('../controllers/tasks.controller');

router.get('/:board_id/tasks', getAllTasks);

router.get('/:board_id/tasks/:task_id', getTaskById);

router.post('/:board_id/tasks', postNewTask);

router.put('/:board_id/tasks/:task_id', updateTaskInfo);

router.delete('/:board_id/tasks/:task_id', deleteTask);

router.patch('/:board_id/tasks/:task_id/status', setNewTaskStatus);

router.post('/:board_id/tasks/:task_id/archive', archiveTask);

router.post('/:board_id/tasks/:task_id/comments', addTaskComment);

router.delete(
	'/:board_id/tasks/:task_id/comments/:comment_id',
	deleteTaskComment
);

module.exports = router;
