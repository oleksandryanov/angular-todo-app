const express = require('express');
const { check } = require('express-validator');

const router = express.Router();

const {
	getAllBoards,
	getBoardById,
	postNewBoard,
	deleteBoard,
	updateBoardInfo,
	changeColumnColor,
} = require('../controllers/boards.controller');

router.get('/', getAllBoards);

router.get('/:board_id', getBoardById);

router.post(
	'/',
	[
		check('name', 'Incorrect name')
			.isString()
			.isLength({ min: 4, max: 16 }),
		check('description', 'Incorrect description').isString(),
	],
	postNewBoard
);

router.put(
	'/:board_id',
	[check('name', 'Incorrect name').isString().isLength({ min: 4, max: 16 })],
	updateBoardInfo
);

router.delete('/:board_id', deleteBoard);

router.patch(
	'/:board_id/column_color',
	[
		check('column', 'Incorrect column').isIn(['TD', 'IP', 'D']),
		check('color', 'Color need to be provided').exists(),
	],
	changeColumnColor
);

module.exports = router;
